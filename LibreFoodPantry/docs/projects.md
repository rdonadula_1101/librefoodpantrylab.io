---
id: projects
title: Projects
sidebar_label: Projects
---

All LibreFoodPantry source code is kept on GitLab projects under the [LibreFoodPantry](https://gitlab.com/LibreFoodPantry) GitLab group. Projects are organized into subgroups. Below is a rough sketch of that organization.


## The [Client Solutions](https://gitlab.com/LibreFoodPantry/client-solutions) Group

Projects related to specific clients, are found in Client Solutions. Currently (Feb. 2021) we have the following clients.

* Bear Necessities Market - A food pantry located at Western New England University (WNE).
* NEST - A food pantry located at Nassau Community College (NCC).
* Thea's Pantry - A food pantry located at Worcester State University.


## The [Common Services](https://gitlab.com/LibreFoodPantry/common-services) Group

Projects that are general and may server more than one client are found in Common Services.


## Other Projects

Other projects support the infrastructure of the LibreFoodPantry community (e.g., this website).
