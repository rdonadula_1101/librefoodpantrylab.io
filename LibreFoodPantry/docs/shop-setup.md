---
id: shop-setup
title: Shop Setup
sidebar_label: Shop Setup
---

:::caution
This document is out of date, but may not be entirely obsolete.
:::

:::caution
This document may not apply to every shop. Please talk to your shop manager to determine if this document applies to your shop.
:::


This document is a guide for Shop Managers on setting up their shop under LibreFoodPantry (LFP) on GitLab Gold. A shop consists of the following artifacts:

* A shop group that is a subgroup of LFP on GitLab.
* Shop forks under the shop group,
  which are forks of LFP projects that the shop is working on.
* A shop board, and a corresponding label, in the LFP group for coordinating members of the shop.
* Zero or more team boards, and corresponding labels, in the LFP group for coordinating members of a team.


## Create the shop's subgroup


1. A Trustee, in the LFP group, adds the Shop Manager as a `maintainer`.

2. A Trustee, in the LFP group, creates a shop subgroup, following the naming convention `Shop-NAME`.

3. A Trustee, in the shop's subgroup, gives the Shop Manager the role of `owner`.

4. The Shop Manager, in the shop's subgroup, adds each shop member as a `developer`.

### Users & permissions diagram:
![GitLab Shop Board Diagram](shop-setup-diagrams/GitLab_Gold_Setup_Option1_Users_And_Permissions.png)


## Create the shop's issue board

The shop's issue board is used by the shop to coordinate their efforts.
Each shop has a board in the LFP group so that they can visualize issues from any project in LFP. This allows shops to more easily work on multiple projects in LFP.

1. The Shop Manager, in the [LFP's label page](https://gitlab.com/groups/LibreFoodPantry/-/labels), creates a shop label, following the naming convention `Shop-NAME`.

2. The Shop Manager, in the [LFP's boards page](https://gitlab.com/groups/LibreFoodPantry/-/boards/1274333), create a new shop issue board, following the naming convention `Shop-NAME`. Filter the board by the shop's label by clicking `Edit board` and then adding your shop's label. That way only issues labeled with the shop's label will appear on the shop's board.

3. The Shop Manager, in the shop's board, adds lists for each of the `flow::*` labels (as the shop manager sees fit). Arrange the lists from left to right in the following order.

  1. `flow::backlog`
  2. `flow::in progress`
  3. `flow::task to do`
  4. `flow::doing task`
  5. `flow::needs review`
  6. `flow::needs merge`

### Shop LFP-level issue board screenshot:
![GitLab Shop Board Screenshot](shop-setup-diagrams/shop-board-screenshot.PNG)


## Create a team's issue boards

If the Shop Manager would like each team in their shop to have a team issue board, follow the same directions for setting up the shop's issue board, use the naming convention `Shop-NAME-TEAM` for the team label and board name. This will help associate the team with the shop.


## Setting up shop forks

For each LFP project the shop will work on, the shop needs a fork of the project that its members can push changes to. This section describes how to create and configure each fork.

1. The Shop Manager, in the LFP project, forks the LFP project into the shop's subgroup.

2. The Shop Manager, in the fork, enables repository mirroring as follows:

  1. In your forked GitLab project go to `Settings`, `Repository`, `Mirroring repositories`.
  2. Under `Mirror a repository` paste the URL for the original LFP project you forked into the `Input your repository URL` box. Use the https version of the "clone" URL.
  3. Select `Pull` for `Mirror direction`.
  4. Enable `Overwrite diverged branches`.
  5. Enable `Only mirror protected branches`.
  6. Click the `Mirror repository` button.

  ![Repository mirroring screenshot](shop-setup-diagrams/repository-mirroring.PNG)

  By enabling repository mirroring, any changes to the original LFP project's master branch will automatically be pulled to your forked shop project.
  This keeps your shop's master branch up-to-date with the upstream and lets your shop developers pull changes from your shop fork instead of the LFP upstream, simplifying the branch-synchronization process.

3. The Shop Manager, in the fork, turns off the issue tracker for your forked project by opening your project and going to `Settings`, `General`, expand `Visibility, project features, permissions` and disable `Issues` then click `Save changes`.

  ![Disable issues screenshot](shop-setup-diagrams/disable-issues.PNG)

4. The Shop Manager, in the fork, disables push and merge to master by going to `Settings`, `Repository` expand `Protected Branches` and select `No one` for `Allowed to merge` and `Maintainers` for `Allowed to push` for the `master` branch. Although `Maintainers` are allowed to push to master, they should not. This permission is given so that mirroring can work correctly. All changes should be pushed to feature branches and merged into LFP's upstream.

  ![Disable merging and pushing to master screenshot](shop-setup-diagrams/protected-branches.PNG)


### GitLab Gold subgroup groups and projects diagram:

  ![GitLab Gold Option 1 Groups & Projects Diagram](shop-setup-diagrams/GitLab_Gold_Setup_Option1_Groups_And_Projects.png)


## Adding shop members as `reporter` to upstream project.

To allow shop members the ability to manage issues in a project's issue tracker, shop members must be added to the project as a `reporter`. Shop Managers should do this for each project their shop is working on.

1. The Shop Manager, in the LFP upstream project (**not the fork**), invites the shop group as `reporter` members.
